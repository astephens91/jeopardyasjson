const fs = require('fs')
const axios = require("axios")

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0' 

const jServiceBaseURL = 'https://jservice.xyz/api'

function downloadAllCategories () {
  console.info('\nDownloading all categories...\n')
  axios.get(jServiceBaseURL + '/categories')
    
    .then(res => {
      console.log(res)
      const filepath = 'categories.json'
      const data = res.data
      fs.writeFileSync(filepath, JSON.stringify(data), 'utf8', error => {
        if (error) console.error(error)
      })
      console.info('\nDownload complete.\n')
    })
}

downloadAllCategories()

// We also found this article's examples useful for reading a file:
// https://code-maven.com/reading-a-file-with-nodejs